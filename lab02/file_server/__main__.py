import argparse
import socket
import json
import binascii
from loguru import logger
import struct

from file_client_server_base.message_type import MessageMetadata

BUFFER_SIZE = 100


def parse_args(args=None):
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description="Calculator server",
        add_help=True,
    )

    parser.add_argument("-port", type=int, metavar="PORT", default=37712, help="port for start server")
    return parser.parse_args(args)

def get_crc_checksum(data: bytes) -> str:
    return "%08X" % (binascii.crc32(data) & 0xFFFFFFFF)

def main():
    """Main entrypoint."""
    args = parse_args()
    logger.info('Start server on port {}', args.port)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(('0.0.0.0', args.port))

    try:
        while True:
            logger.info('Waiting for clients')
            data_recived, addr = sock.recvfrom(1024)
            logger.debug('Request recived ({}) from client {}', data_recived, addr)

            try:
                message_metadata = MessageMetadata.parse_obj(json.loads(data_recived))
            except BaseException as e:
                logger.error(repr(e))
                sock.sendto(b'E' + repr(e).encode(), addr)

            # Send buffer_size
            logger.debug('Sending buffer_size={} to client {}', BUFFER_SIZE, addr)
            sock.sendto(b'S' + struct.pack('<I', BUFFER_SIZE), addr)

            data = []
            data_len = 0
            while data_len < message_metadata.size:
                msg, addr2 = sock.recvfrom(BUFFER_SIZE)
                if addr2 != addr:
                    raise NotImplementedError('Many clients is not supported yet')
                data_len += len(msg)
                data.append(msg)

            logger.debug('Recived file data (len={}, parts={})', data_len, len(data))

            file_data = b''.join(data)
            assert len(file_data) == data_len

            if message_metadata.checksum != get_crc_checksum(file_data):
                logger.error('Checksum is not the same. Skip file')
                continue
            else:
                logger.debug('Checksum is the same')

            # send OK to client
            sock.sendto(b'OK', addr)

            file_name = f"new_{message_metadata.name}"
            logger.debug('Save data to file {}', file_name)
            with open(file_name, 'wb') as f:
                f.write(file_data)
            logger.info('Recived file is saved to {}', file_name)

    except KeyboardInterrupt:
        logger.error('KeyboardInterrupt. Stop server')
        pass

if __name__ == "__main__":
    main()
