from pydantic import BaseModel

class MessageMetadata(BaseModel):
    name: str
    checksum: str
    size: int
