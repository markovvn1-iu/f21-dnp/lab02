import argparse
import socket
from loguru import logger


def parse_args(args=None):
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description="Calculator client",
        add_help=True,
    )

    parser.add_argument("-addr", type=str, metavar="ADDR", default='localhost', help="address of the server")
    parser.add_argument("-port", type=int, metavar="PORT", default=37711, help="port of the server")
    return parser.parse_args(args)

def main():
    """Main entrypoint."""
    args = parse_args()

    addr = (args.addr, args.port)
    logger.info('Start client on {}', addr)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    try:
        while True:
            cmd = input('Input command for calculation: ')
            if cmd.lower() == 'quit':
                logger.info('User has quit')
                return
            if not cmd: continue

            data_to_send = cmd.encode()
            sock.sendto(data_to_send, addr)
            logger.debug('Data was sended (len={})', len(data_to_send))

            data_recived, _ = sock.recvfrom(1024)
            logger.debug('Data recived (len={})', len(data_recived))
            print(f'Result: {data_recived.decode()}')
    except KeyboardInterrupt:
        logger.error('KeyboardInterrupt. Stop client')
        pass


if __name__ == "__main__":
    main()
