
from typing import Union
from loguru import logger


class ControllerException(Exception):
    pass

class ParseMessageError(ControllerException):
    pass

class CalculationError(ControllerException):
    pass



class Controller:

    calc_func = {
        '*': lambda a, b: a * b,
        '/': lambda a, b: a / b,
        '-': lambda a, b: a - b,
        '+': lambda a, b: a + b,
        '>': lambda a, b: a > b,
        '<': lambda a, b: a < b,
        '>=': lambda a, b: a >= b,
        '<=': lambda a, b: a <= b,
    }

    def parse_number(self, number_str: str) -> Union[int, str]:
        try:
            return int(number_str)
        except ValueError:
            pass

        try:
            return float(number_str)
        except ValueError:
            raise ParseMessageError(f"Cannot cast '{number_str}' to int or to float")
            

    def process_message(self, msg: str) -> str:
        data = msg.strip().split()
        if len(data) != 3:
            raise ParseMessageError('There are more than 3 items')
        
        cmd = data[0]
        if cmd not in self.calc_func:
            raise ParseMessageError(f"Unknown operand '{cmd}'")

        a = self.parse_number(data[1])
        b = self.parse_number(data[2])

        logger.debug('Parsed task: {} {} {}', cmd, a, b)

        try:
            res = self.calc_func[cmd](a, b)
        except BaseException as e:
            raise CalculationError(repr(e))

        return str(res)