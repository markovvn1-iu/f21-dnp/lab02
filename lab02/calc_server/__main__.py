import argparse
import socket
from .controller import Controller, ControllerException
from loguru import logger

controller = Controller()

def parse_args(args=None):
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description="Calculator server",
        add_help=True,
    )

    parser.add_argument("-port", type=int, metavar="PORT", default=37711, help="port for start server")
    return parser.parse_args(args)

def main():
    """Main entrypoint."""
    args = parse_args()
    logger.info('Start server on port {}', args.port)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(('0.0.0.0', args.port))

    try:
        while True:
            data_recived, addr = sock.recvfrom(1024)
            logger.debug('Request recived ({}) form client {}', data_recived, addr)

            try:
                res = controller.process_message(data_recived.decode())
            except ControllerException as e:
                sock.sendto(f"Error: {repr(e)}".encode(), addr)
                logger.error('Error: {}', repr(e))
            except BaseException as e:
                sock.sendto("Unknown error".encode(), addr)
                logger.error('Unknown error: {}', repr(e))
            else:
                data_to_send = res.encode()
                sock.sendto(data_to_send, addr)
                logger.debug('Response ({}) was sended to client', data_to_send, addr)
    except KeyboardInterrupt:
        logger.error('KeyboardInterrupt. Stop server')
        pass

if __name__ == "__main__":
    main()
