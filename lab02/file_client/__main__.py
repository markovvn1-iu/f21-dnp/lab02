import argparse
import socket
import binascii
import struct
from loguru import logger

from file_client_server_base.message_type import MessageMetadata


def parse_args(args=None):
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description="File sender client",
        add_help=True,
    )

    parser.add_argument("file_name", type=str, metavar="FILE_NAME", help="name of file to send")
    parser.add_argument("-addr", type=str, metavar="ADDR", default='localhost', help="address of the server")
    parser.add_argument("-port", type=int, metavar="PORT", default=37712, help="port of the server")
    return parser.parse_args(args)


def get_crc_checksum(data: bytes) -> str:
    return "%08X" % (binascii.crc32(data) & 0xFFFFFFFF)


def main():
    """Main entrypoint."""
    args = parse_args()

    try:
        with open(args.file_name, 'rb') as f:
            file_data = f.read()
    except BaseException as e:
        logger.critical(repr(e))
        return

    msg = MessageMetadata(
        name=args.file_name,
        checksum=get_crc_checksum(file_data),
        size=len(file_data),
    )

    addr = (args.addr, args.port)
    logger.info('Start client on {}', addr)
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(1)

    message_metadata = msg.json()

    try:
        while True:
            logger.debug('Sending massage metadata ({}) to server', message_metadata)
            sock.sendto(message_metadata.encode(), addr)

            try:
                data, _ = sock.recvfrom(1024)
            except socket.timeout:
                logger.error('Server did not send any data in 1 second. Probably The server isn’t available. Shutting down')
                return

            if data.startswith(b'E'):
                logger.error('Recived error message: {}', data.decode())
                continue
            elif not data.startswith(b'S') or len(data) != 5:
                logger.error('Recived unknown message: {}', data[1:])
                continue

            buffer_size = struct.unpack('<I', data[1:])[0]
            logger.debug('Recived buffer_size={}', buffer_size)

            logger.debug('Sending image data (len={})', len(file_data))
            for i in range(0, len(file_data), buffer_size):
                sock.sendto(file_data[i:i+buffer_size], addr)

            try:
                data, _ = sock.recvfrom(1024)
            except socket.timeout:
                logger.error("Server did not send 'OK' in 1 second. Repeating...")
                continue

            if data != b'OK':
                logger.error("Server send something strange: {}", data)
                continue
            else:
                logger.info("File sent successfully")
                return
    except KeyboardInterrupt:
        logger.error('KeyboardInterrupt. Stop client')
        pass


if __name__ == "__main__":
    main()
