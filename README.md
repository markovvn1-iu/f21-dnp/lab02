# Lab 02

Simple client-server calculator

### Download and create venv

1. Download repo: `git clone https://gitlab.com/markovvn1-iu/f21-dnp/lab02.git`
2. Open downloaded folder: `cd lab02`
3. Create python virtual environment: `make venv`

### Main task (calculator)

1. Start server: `make calc_server_up`
2. Run client: `make calc_run_client`
3. Have fun!

### Additional task (send files)

1. Start server: `make file_server_up`
2. Run client: `make file_run_client`
3. Have fun!