VENV ?= .venv

.PHONY: venv
venv:
	python -m venv $(VENV)
	$(VENV)/bin/python -m pip install --upgrade pip
	$(VENV)/bin/python -m pip install poetry
	$(VENV)/bin/poetry install

.PHONY: calc_run_client
calc_run_client:
	$(VENV)/bin/python -m calc_client

.PHONY: calc_server_up
calc_server_up:
	$(VENV)/bin/python -m calc_server

.PHONY: file_run_client
file_run_client:
	$(VENV)/bin/python -m file_client innopolis.jpg

.PHONY: file_server_up
file_server_up:
	$(VENV)/bin/python -m file_server